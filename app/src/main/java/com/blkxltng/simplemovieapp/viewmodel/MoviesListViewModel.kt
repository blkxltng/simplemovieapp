package com.blkxltng.simplemovieapp.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.blkxltng.simplemovieapp.movies.model.Movie
import com.blkxltng.simplemovieapp.movies.repository.MovieListRequest
import com.blkxltng.simplemovieapp.movies.usecase.MovieListType
import com.blkxltng.simplemovieapp.movies.usecase.MovieListUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MoviesListViewModel @Inject constructor(
    private val movieListUseCase: MovieListUseCase
): ViewModel() {

    private val _moviesListStateFlow = MutableStateFlow(listOf<Movie>())
    val moviesListStateFlow: StateFlow<List<Movie>> = _moviesListStateFlow
    init {
    }

    fun getMoviesList(type: MovieListType = MovieListType.POPULAR) {
        viewModelScope.launch {
            movieListUseCase.fetchData(
                MovieListRequest(type = type)
            )
                .flowOn(Dispatchers.IO)
                .catch {
                    Log.e(MoviesListViewModel::class.simpleName, it.message.toString())
                }
                .collect {
                    Log.d(MoviesListViewModel::class.simpleName, it.toString())
                    _moviesListStateFlow.emit(it)
            }
        }
    }
}