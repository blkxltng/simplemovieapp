package com.blkxltng.simplemovieapp.di

import com.blkxltng.simplemovieapp.BASE_URL
import com.blkxltng.simplemovieapp.movies.datasource.MovieListDataSource
import com.blkxltng.simplemovieapp.movies.repository.MovieListRepository
import com.blkxltng.simplemovieapp.movies.service.MovieService
import com.blkxltng.simplemovieapp.movies.usecase.MovieListUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


@Module
@InstallIn(SingletonComponent::class)
class MoviesModule {
    @Provides
    fun provideMoviesService(): MovieService {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(MovieService::class.java)
    }

    @Provides
    fun providePopularMovieDataSource(movieService: MovieService): MovieListDataSource {
        return MovieListDataSource(movieService)
    }

    @Provides
    fun providePopularMovieRepository(dataSource: MovieListDataSource): MovieListRepository {
        return MovieListRepository(dataSource)
    }

    @Provides
    fun providePopularMovieUseCase(repository: MovieListRepository): MovieListUseCase {
        return MovieListUseCase(repository)
    }
}