package com.blkxltng.simplemovieapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SimpleMovieApplication: Application() {
}