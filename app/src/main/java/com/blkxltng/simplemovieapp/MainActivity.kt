package com.blkxltng.simplemovieapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.blkxltng.simplemovieapp.movies.usecase.MovieListType
import com.blkxltng.simplemovieapp.ui.components.MoviesListView
import com.blkxltng.simplemovieapp.ui.theme.SimpleMovieAppTheme
import com.blkxltng.simplemovieapp.viewmodel.MoviesListViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity: ComponentActivity() {

    private val viewmodel: MoviesListViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SimpleMovieAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MoviesListView(viewModel = viewmodel)
                }
            }
        }
        viewmodel.getMoviesList(MovieListType.TOP_RATED)
    }
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    SimpleMovieAppTheme {

    }
}