package com.blkxltng.simplemovieapp

interface UseCase<T,K> {
    suspend fun fetchData(model: T): K
}

interface Repository<T,K> {
    suspend fun getData(model: T): K
}

interface DataSource<T,K> {
    suspend fun getData(model: T): K
}
