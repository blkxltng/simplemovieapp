package com.blkxltng.simplemovieapp.movies.service

import com.blkxltng.simplemovieapp.movies.model.MovieDetailResponse
import com.blkxltng.simplemovieapp.movies.model.MoviesResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieService {
    @GET("movie/popular")
    suspend fun getPopularMoviesList(
        @Query("language") language: String = "en-US",
        @Query("page") pageNumber: Int = 1,
        @Query("api_key") apiKey: String
    ): MoviesResponse

    @GET("movie/now_playing")
    suspend fun getNowPlayingMoviesList(
        @Query("language") language: String = "en-US",
        @Query("page") pageNumber: Int = 1,
        @Query("api_key") apiKey: String
    ): MoviesResponse

    @GET("movie/top_rated")
    suspend fun getTopRatedMoviesList(
        @Query("language") language: String = "en-US",
        @Query("page") pageNumber: Int = 1,
        @Query("api_key") apiKey: String
    ): MoviesResponse

    @GET("movie/upcoming")
    suspend fun getUpcomingMoviesList(
        @Query("language") language: String = "en-US",
        @Query("page") pageNumber: Int = 1,
        @Query("api_key") apiKey: String
    ): MoviesResponse

    @GET("movie/{movie_id}")
    suspend fun getMovieDetails(
        @Path("movie_id") moveId: Int,
        @Query("api_key") apiKey: String
    ): MovieDetailResponse
}