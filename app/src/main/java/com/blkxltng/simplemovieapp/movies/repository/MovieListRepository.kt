package com.blkxltng.simplemovieapp.movies.repository

import com.blkxltng.simplemovieapp.Repository
import com.blkxltng.simplemovieapp.movies.datasource.MovieListDataSource
import com.blkxltng.simplemovieapp.movies.model.Movie
import com.blkxltng.simplemovieapp.movies.usecase.MovieListType
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class MovieListRepository @Inject constructor(
    private val movieDataSource: MovieListDataSource
): Repository<MovieListRequest, Flow<List<Movie>>> {
    override suspend fun getData(model: MovieListRequest): Flow<List<Movie>> {
        return movieDataSource.getData(model).map {
            it.results
        }
    }
}

data class MovieListRequest(
    val language: String = "en-US",
    val page: Int = 1,
    val type: MovieListType = MovieListType.POPULAR
)
