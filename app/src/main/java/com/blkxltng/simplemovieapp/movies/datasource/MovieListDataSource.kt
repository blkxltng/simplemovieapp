package com.blkxltng.simplemovieapp.movies.datasource

import com.blkxltng.simplemovieapp.API_KEY
import com.blkxltng.simplemovieapp.DataSource
import com.blkxltng.simplemovieapp.movies.model.MoviesResponse
import com.blkxltng.simplemovieapp.movies.repository.MovieListRequest
import com.blkxltng.simplemovieapp.movies.service.MovieService
import com.blkxltng.simplemovieapp.movies.usecase.MovieListType
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import javax.inject.Inject

class MovieListDataSource @Inject constructor(
    private val movieService: MovieService
): DataSource<MovieListRequest, Flow<MoviesResponse>> {
    override suspend fun getData(model: MovieListRequest): Flow<MoviesResponse> {
        return flowOf(
            when (model.type) {
                MovieListType.POPULAR -> movieService.getPopularMoviesList(model.language, model.page, API_KEY)
                MovieListType.NOW_PLAYING -> movieService.getNowPlayingMoviesList(model.language, model.page, API_KEY)
                MovieListType.TOP_RATED -> movieService.getTopRatedMoviesList(model.language, model.page, API_KEY)
                MovieListType.UPCOMING -> movieService.getUpcomingMoviesList(model.language, model.page, API_KEY)
            }
        )
    }
}