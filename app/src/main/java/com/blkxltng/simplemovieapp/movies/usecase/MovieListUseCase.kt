package com.blkxltng.simplemovieapp.movies.usecase

import com.blkxltng.simplemovieapp.UseCase
import com.blkxltng.simplemovieapp.movies.model.Movie
import com.blkxltng.simplemovieapp.movies.repository.MovieListRepository
import com.blkxltng.simplemovieapp.movies.repository.MovieListRequest
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class MovieListUseCase @Inject constructor(
    private val movieRepository: MovieListRepository
): UseCase<MovieListRequest, Flow<List<Movie>>> {
    override suspend fun fetchData(model: MovieListRequest): Flow<List<Movie>> {
        return movieRepository.getData(model)
    }
}

enum class MovieListType {
    POPULAR,
    NOW_PLAYING,
    UPCOMING,
    TOP_RATED
}
