package com.blkxltng.simplemovieapp.movies.model.movieDetails

data class Genre(
    val id: Int,
    val name: String
)