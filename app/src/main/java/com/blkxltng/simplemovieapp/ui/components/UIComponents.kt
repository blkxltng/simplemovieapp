package com.blkxltng.simplemovieapp.ui.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.blur
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.NestedScrollSource
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.blkxltng.simplemovieapp.BASE_IMAGE_URL
import com.blkxltng.simplemovieapp.movies.model.Movie
import com.blkxltng.simplemovieapp.viewmodel.MoviesListViewModel


@Composable
fun MoviesListView(viewModel: MoviesListViewModel) {

    var movies = remember {
        mutableStateListOf<Movie>()
    }
    LaunchedEffect(key1 = movies) {
        viewModel.moviesListStateFlow.collect {
//            movies.clear()
            movies.addAll(it)
        }
    }

    val nestedScrollConnection = remember {
        object : NestedScrollConnection {
            override fun onPreScroll(available: Offset, source: NestedScrollSource): Offset {
                val delta = available.y
                // called when you scroll the content
                return Offset.Zero
            }
        }
    }

    LazyVerticalGrid(
        columns = GridCells.Fixed(2),
        modifier = Modifier.fillMaxSize().nestedScroll(nestedScrollConnection)
    ) {
        items(movies) {
            MovieListItem(movie = it)
        }
    }
}

@Composable
fun MovieListItem(movie: Movie) {
    Box {
        AsyncImage(
            model = "$BASE_IMAGE_URL${movie.poster_path}",
            contentDescription = "Movie Image",
            modifier = Modifier.fillMaxSize().blur(2.dp),
            contentScale = ContentScale.Crop
        )
        Text(
            text = movie.title,
            style = TextStyle(
                color = Color.White,
                fontSize = 16.sp
            ),
            modifier = Modifier.align(Alignment.BottomStart)
        )
    }
}

@Composable
@Preview(showBackground = true)
fun MovieItemPreview() {
//    MovieListItem()
}
